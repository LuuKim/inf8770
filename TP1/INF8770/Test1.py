import random
import numpy as np
import matplotlib.pyplot as plt


#  * Génère une chaîne de caractères aléatoirement
def randString(n, letters):
    return ''.join((random.choice(letters.upper()) for i in range(n)))


#  * Retourne le nombre de caractères consécutifs le plus long dans un string
#  https://www.geeksforgeeks.org/maximum-consecutive-repeating-character-string/
def maxRepeating(str):
    l = len(str)
    count = 0

    # Find the maximum repeating
    # character starting from str[i]
    res = str[0]
    for i in range(l):

        cur_count = 1
        for j in range(i + 1, l):

            if (str[i] != str[j]):
                break
            cur_count += 1

        # Update result if required
        if cur_count > count:
            count = cur_count
            res = str[i]
    return count


#  * Retourne le nombre de bits nécessaires pour écrire un number
#  https://www.geeksforgeeks.org/python-program-to-covert-decimal-to-binary-number/
def countBits(n):
    return len(bin(n).replace("0b", ""))


#  * Codage par plage
#  https://github.com/gabilodeau/INF8770/blob/master/Codage%20par%20plage.ipynb

def codageParPlage(Message, cpt):
    compteur = cpt
    dictsymb = [Message[0]]
    dictbin = ["{:b}".format(0)]
    nbsymboles = 1
    for i in range(1, len(Message)):
        if Message[i] not in dictsymb:
            dictsymb += [Message[i]]
            dictbin += ["{:b}".format(nbsymboles)]
            nbsymboles += 1

    longueurOriginale = np.ceil(np.log2(nbsymboles)) * len(Message)  # Longueur du message avec codage binaire
    for i in range(nbsymboles):
        dictbin[i] = "{:b}".format(i).zfill(int(np.ceil(np.log2(nbsymboles))))

    dictsymb.sort()
    i = 0;
    MessageCode = []
    longueur = 0
    while i < len(Message):
        carac = Message[i]  # caractere qui sera codé
        repetition = 1
        # Calcul le nombre de répétitions.
        i += 1
        # tient compte de la limite du compteur
        while i < len(Message) and repetition < 2 ** compteur and Message[i] == carac:
            i += 1
            repetition += 1
        # Codage à l'aide du dictionnaire
        coderepetition = "{:b}".format(repetition - 1).zfill(compteur)
        codebinaire = dictbin[dictsymb.index(carac)]
        MessageCode += [coderepetition, codebinaire]
        longueur += len(codebinaire) + len(coderepetition)
    originales.append(longueurOriginale)
    RLEcompressed.append(longueur)

def codageLZW(Message):
    dictsymb = [Message[0]]
    dictbin = ["{:b}".format(0)]
    nbsymboles = 1
    for i in range(1, len(Message)):
        if Message[i] not in dictsymb:
            dictsymb += [Message[i]]
            dictbin += ["{:b}".format(nbsymboles)]
            nbsymboles += 1

    longueurOriginale = np.ceil(np.log2(nbsymboles)) * len(Message)
    for i in range(nbsymboles):
        dictbin[i] = "{:b}".format(i).zfill(int(np.ceil(np.log2(nbsymboles))))

    dictsymb.sort()
    i = 0;
    MessageCode = []
    longueur = 0
    while i < len(Message):
        precsouschaine = Message[i]  # sous-chaine qui sera codé
        souschaine = Message[i]  # sous-chaine qui sera codé + 1 caractère (pour le dictionnaire)

        # Cherche la plus grande sous-chaine. On ajoute un caractère au fur et à mesure.
        while souschaine in dictsymb and i < len(Message):
            i += 1
            precsouschaine = souschaine
            if i < len(Message):  # Si on a pas atteint la fin du message
                souschaine += Message[i]

                # Codage de la plus grande sous-chaine à l'aide du dictionnaire
        codebinaire = [dictbin[dictsymb.index(precsouschaine)]]
        MessageCode += codebinaire
        longueur += len(codebinaire[0])
        # Ajout de la sous-chaine codé + symbole suivant dans le dictionnaire.
        if i < len(Message):
            dictsymb += [souschaine]
            dictbin += ["{:b}".format(nbsymboles)]
            nbsymboles += 1

        # Ajout de 1 bit si requis
        if np.ceil(np.log2(nbsymboles)) > len(MessageCode[-1]):
            for j in range(nbsymboles):
                dictbin[j] = "{:b}".format(j).zfill(int(np.ceil(np.log2(nbsymboles))))

    LZWcompressed.append(longueur)


#  * Affiche un diagramme
#   https://python-graph-gallery.com/11-grouped-barplot/

def showDiagram():
    # set width of bar
    barWidth = 0.25

    # Set position of bar on X axis
    r1 = np.arange(len(originales))
    r2 = [x + barWidth for x in r1]
    r3 = [x + barWidth for x in r2]

    # Make the plot
    plt.bar(r1, originales, color='black', width=barWidth, edgecolor='white', label='To')
    plt.bar(r2, RLEcompressed, color='blue', width=barWidth, edgecolor='white', label='Tc_RLE')
    plt.bar(r3, LZWcompressed, color='red', width=barWidth, edgecolor='white', label='Tc_LZW')

    # Add xticks on the middle of the group bars
    plt.xlabel('Essai', fontweight='bold')
    plt.ylabel('Longueur', fontweight='bold')
    plt.title("Test 1", fontsize=16, fontweight='bold')

    # Create legend & Show graphic
    plt.legend()
    plt.show()

if __name__ == "__main__":
    letters = 'AB'
    probaRLE = 0
    probaLZW = 0
    originales = []
    RLEcompressed = []
    LZWcompressed = []
    probaRLEplusPerfomant = 0
    r = 50
    for x in range(0, r):
        n = random.randrange(5, 10)
        Message = randString(n, letters)
        nbBits = countBits(maxRepeating(Message))
        codageParPlage(Message, nbBits)
        if RLEcompressed[x] < originales[x]:
            probaRLE += 1
        codageLZW(Message)
        if LZWcompressed[x] < originales[x]:
            probaLZW += 1
        if RLEcompressed[x] < LZWcompressed[x] :
            probaRLEplusPerfomant += 1

    probaRLE /= r
    probaLZW /= r
    probaRLEplusPerfomant /= r
    print("Probabilité que RLE soit plus performant que LZW ", probaRLEplusPerfomant)
    print("ProbaRLE To > Tc ", probaRLE)
    print("ProbaLZW To > Tc ", probaLZW)
    showDiagram()
