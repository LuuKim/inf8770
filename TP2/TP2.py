# coding: latin-1
########################## IMPORTS #########################
import math
import os
import matplotlib

from Huffman import Huffman
from RLE import RLE

matplotlib.use('TkAgg')
import numpy as np
import scipy.fftpack as dctpack
from PIL import Image
from Utils import *

####################### CONSTANTES ###############################

ADJUST_COLOR = 128
MAX_COLOR = 255
AMOUNT_Y = AMOUNT_CB = AMOUNT_CR = 0
BLOC_SIZE = 8
SIZE = 8
BLOC_Y = BLOC_CB = BLOC_CR = []
Blocs = []
dc = ac = 0
HALF_CHROMA = 2
imgeType = Image.ANTIALIAS
TYPE_UINT = np.uint8
# https://github.com/gabilodeau/INF8770/blob/master/Transformee%20DCT%20et%20quantification.ipynb
QUANTIFICATION_MATRIX = np.array([[16, 11, 10, 16, 24, 40, 51, 61],
                                 [12, 12, 14, 19, 26, 58, 60, 55],
                                 [14, 13, 16, 24, 40, 57, 69, 56],
                                 [14, 17, 22, 29, 51, 87, 80, 62],
                                 [18, 22, 37, 56, 68, 109, 103, 77],
                                 [24, 35, 55, 64, 81, 104, 103, 92],
                                 [49, 64, 78, 77, 103, 121, 120, 101],
                                 [72, 92, 95, 98, 112, 100, 103, 99]])

QUANTIFICATION_MATRIX_2 = np.array([[53, 37, 33, 53, 80, 133, 170, 203],
                                 [40, 40, 47, 63, 87, 193, 200, 183],
                                 [47, 43, 53, 80, 133, 190, 230, 187],
                                 [47, 57, 73, 97, 170, 290, 267, 207],
                                 [60, 73, 123, 187, 227, 363, 343, 257],
                                 [80, 117, 183, 213, 270, 347, 377, 307],
                                 [163, 213, 260, 290, 343, 403, 400, 337],
                                 [240, 307, 317, 327, 373, 333, 343, 300]])

#http://ljk.imag.fr/membres/Valerie.Perrier/SiteWeb/node10.html
QUANTIFICATION_MATRIX_3 = np.array([[4, 7, 10, 13, 16, 19, 22, 25],
                                   [7, 10, 13, 16, 19, 22, 25, 28],
                                   [10, 13, 16, 19, 22, 25, 28, 31],
                                   [13, 16, 19, 22, 25, 28, 31, 34],
                                   [16, 19, 22, 25, 28, 31, 34, 37],
                                   [19, 22, 25, 28, 31, 34, 37, 40],
                                   [22, 25, 28, 31, 34, 37, 40, 43],
                                   [25, 28, 31, 34, 37, 40, 43, 46]])

QUANTIFICATION_MATRIX_4 = np.array([[1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1],
                                    [1, 1, 1, 1, 1, 1, 1, 1]])

QUANTIFICATION_MATRIX_5 = np.array([[200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200],
                                    [200, 200, 200, 200, 200, 200, 200, 200]])

x = QUANTIFICATION_MATRIX


arrayRgb2YCbCr = [[.299, .587, .114], [-.1687, -.3313, .4997], [.4997, -.4187, -.0813]]
arrayYCbCr2Rgb = [[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]]
nameImageOriginal = "big_standard.jpg"


############################################################


# https://stackoverflow.com/questions/34913005/color-space-mapping-ycbcr-to-rgb
def rgb2ycbcr(image):
    imgArray = np.array(image)
    xform = np.array(arrayRgb2YCbCr)
    ycbcr = imgArray.dot(xform.T)
    ycbcr = ycbcr.astype(TYPE_UINT)
    ycbcr[:, :, [1, 2]] += ADJUST_COLOR
    return Image.fromarray(ycbcr, 'RGB')


# https://stackoverflow.com/questions/34913005/color-space-mapping-ycbcr-to-rgb
def ycbcr2rgb(im):
    xform = np.array(arrayYCbCr2Rgb)
    rgb = im.astype(np.float)
    rgb[:, :, [1, 2]] -= ADJUST_COLOR
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb > MAX_COLOR, MAX_COLOR)
    np.putmask(rgb, rgb < 0, 0)
    return TYPE_UINT(rgb)


# https://medium.com/@sddkal/chroma-subsampling-in-numpy-47bf2bb5af83
def chroma(arrayYCbCr):
    arrayYCbCr[1::2, :] = arrayYCbCr[::2, :]
    # Vertically, every second element equals to element above itself.
    arrayYCbCr[:, 1::2] = arrayYCbCr[:, ::2]
    # Horizontally, every second element equals to the element on its left side.
    print(arrayYCbCr)
    return arrayYCbCr

def splitIntoBlock16x16(imgChroma):
    split = []
    Y = imgChroma[0]
    CB = imgChroma[1]
    CR = imgChroma[2]
    blocindexY = int(math.floor(Y.shape[0] / SIZE))
    blocindexC = int(math.floor(Y.shape[0] / SIZE))
    Ybloc = []
    CBbloc = []
    CRbloc = CBbloc
    for w in range(blocindexY):
        for h in range(blocindexY):
            Ybloc[w * blocindexY + h] = Y[w * SIZE:w * SIZE + SIZE, h * SIZE:h * SIZE + SIZE]
    for w in range(blocindexC):
        for h in range(blocindexC):
            CBbloc[w * blocindexC + h] = CB[w * SIZE:w * SIZE + SIZE, h * SIZE:h * SIZE + SIZE]
            CRbloc[w * blocindexC + h] = CR[w * SIZE:w * SIZE + SIZE, h * SIZE: h * SIZE + SIZE]
    split.append(Ybloc)
    split.append(CBbloc)
    split.append(CRbloc)
    return split


def applyTransformation():
    imgYCBCR = rgb2ycbcr(Image.open(nameImageOriginal))
    #imgYCBCR.show()
    w0, h0 = imgYCBCR.size
    Y1 , CB1, CR1 = imgYCBCR.split()
    temp = np.array(CB1)
    AMOUNT_Y = Y1.size
    w1, h1 = temp.shape
    temp, CB2, CR2 = imgYCBCR.resize([int(w0/2), int(h0/2)], imgeType).split()
    for w in range(0, w1, HALF_CHROMA):
        for h in range(0, h1, HALF_CHROMA):
            np.array(CB2)[int(math.floor(w/HALF_CHROMA))][int(math.floor(h/HALF_CHROMA))] \
                = np.mean(np.array(CB1)[w:w+HALF_CHROMA, h:h+HALF_CHROMA])
            np.array(CR2)[int(math.floor(w/HALF_CHROMA))][int(math.floor(h/HALF_CHROMA))]\
                = np.mean(np.array(CR1)[w:w + HALF_CHROMA, h:h + HALF_CHROMA])


    TRANFORMATION_Y = []
    TRANFORMATION_CB = []
    TRANFORMATION_CR = []
    TRANFORMATION_Array = []
    AMOUNT_CB = CB2.size
    AMOUNT_CR = CR2.size

    for w in range(0, AMOUNT_Y[0], SIZE):
        for h in range(0, AMOUNT_Y[1], SIZE):
             b = np.array(Y1)[w:w+SIZE, h:h+SIZE]
             YTemp = DCT(b).astype(TYPE_UINT)
             qTemp = quantification(YTemp)
             zTemp = block_to_zigzag(qTemp)
             TRANFORMATION_Y.append(zTemp)
    for w in range(0, AMOUNT_CB[0], SIZE):
        for h in range(0, AMOUNT_CB[1], SIZE):
              b = np.array(CB2)[w:w+SIZE, h:h+SIZE]
              CBTemp = DCT(b).astype(TYPE_UINT)
              qTemp = quantification(CBTemp)
              zTemp = block_to_zigzag(qTemp)
              TRANFORMATION_CB.append(zTemp)
    for w in range(0, AMOUNT_CR[0], SIZE):
        for h in range(0, AMOUNT_CR[1], SIZE):
              b = np.array(CR2)[w:w+SIZE, h:h+SIZE]
              CRTemp = DCT(b).astype(TYPE_UINT)
              qTemp = quantification(CRTemp)
              zTemp = block_to_zigzag(qTemp)
              TRANFORMATION_CR.append(zTemp)

    TRANFORMATION_Array.append(TRANFORMATION_Y)
    TRANFORMATION_Array.append(TRANFORMATION_CB)
    TRANFORMATION_Array.append(TRANFORMATION_CR)


    return TRANFORMATION_Array


def applyIDCT(Blocs):
    for b in Blocs:
        BLOC_Y[b] = IDCT(BLOC_Y)
        BLOC_CB[b] = IDCT(BLOC_CB)
        BLOC_CR[b] = IDCT(BLOC_CR)
    return Blocs


def applyQuantification(DCT_Array):
    Y = DCT_Array[0]
    CB = DCT_Array[1]
    CR = DCT_Array[2]
    Quantif_array= []
    AMOUNT_Y = Y.shape[0]
    AMOUNT_CR = AMOUNT_CB = CB.shape[0]
    QY = np.empty([AMOUNT_Y, SIZE, SIZE])
    QCB = np.empty([AMOUNT_CB, SIZE, SIZE])
    QCR = np.empty([AMOUNT_CR, SIZE, SIZE])
    for w in range(SIZE):
        for h in range(SIZE):
            b = np.array(Y)[w:w + SIZE, h:h + SIZE]
            QY = quantification(b)
    for w in range(SIZE):
        for h in range(SIZE):
            b = np.array(CB)[w:w + SIZE, h:h + SIZE]
            QCB = quantification(b)
    for w in range(SIZE):
        for h in range(SIZE):
            b = np.array(CR)[w:w + SIZE, h:h + SIZE]
            QCR = quantification(b)
    Quantif_array.append(QY)
    Quantif_array.append(QCB)
    Quantif_array.append(QCR)

    return Quantif_array


def applyInvQuantification(img):
    for b in Blocs:
        BLOC_Y = invQuantification(BLOC_Y)
        BLOC_CB = invQuantification(BLOC_CB)
        BLOC_CR = invQuantification(BLOC_CR)
    return img

# https://github.com/ghallak/jpeg-python/blob/2fe1bd2244c3090543695b106866dfa0a3b48f6c/encoder.py
def block_to_zigzag(block):
    return np.array([block[point] for point in zigzag_points(*block.shape)])


# https://github.com/ghallak/jpeg-python/blob/2fe1bd2244c3090543695b106866dfa0a3b48f6c/encoder.py
def quantification(imageBlocDCT):
    return np.round(np.divide(imageBlocDCT, x))

# https://github.com/ghallak/jpeg-python/blob/2fe1bd2244c3090543695b106866dfa0a3b48f6c/encoder.py
def invQuantification(imageBlocDCT):
    return imageBlocDCT * QUANTIFICATION_MATRIX

#https://github.com/gabilodeau/INF8770/blob/master/Transformee%20DCT%20et%20quantification.ipynb
def DCT(bloc):
    # On soustrait 128 pour avoir en signal oscillant autour de z?ro
    #bloc = bloc - 128
    DCT = dctpack.dct(dctpack.dct(bloc.T, norm='ortho').T, norm='ortho')
    np.set_printoptions(precision=2)
    np.set_printoptions(suppress=True)
    return DCT

#https://github.com/gabilodeau/INF8770/blob/master/Transformee%20DCT%20et%20quantification.ipynb
def IDCT(img):
    # On soustrait 128 pour avoir en signal oscillant autour de z?ro
    IdCT = dctpack.idct(dctpack.idct(img[:], axis=0, norm='ortho'), axis=1, norm='ortho')
    np.set_printoptions(precision=2)
    np.set_printoptions(suppress=True)
    img[:] = img[:] + 128
    return IdCT

if __name__ == "__main__":
    #imageJPG = (image.open(nameImageOriginal))

    #imageBis = rgb2ycbcr(imageJPG)

    # Image.fromarray(imageBis).show()

    #imgSplit = splitIntoBlock16x16(chroma(imageBis))

    # Image.fromarray(imgSplit).show()

    #imgdct = applyDCT(imgSplit)

    imgzigzag = applyTransformation()
    To = 0
    Tc = 0
    for i in range(len(imgzigzag)):  # Y, Cb, Cr
        for j in range(len(imgzigzag[i])):  # nb blocs Y, Cb ou Cr
            rle, To_RLE, cpt, dic_RLE = RLE(imgzigzag[i][j])
            h, Tc_H, dic_H = Huffman(rle)

            To += To_RLE
            Tc += Tc_H

    print("To:",To, "Tc:", Tc)
    print((1- (Tc/To))*100)



    # Image.fromarray(imgSplit).show()
    # print("1", imgzigzag[0])
    # print("2", imgzigzag[1])
    # print("3", imgzigzag[2])
    #
    # RLE_Y, To_RLE_Y = RLE.RLE(imgzigzag[0])
    # compressedImg_Y, Tc_Y = Huffman.Huffman(RLE_Y)
    #
    # RLE_Cb, To_RLE_Cb = RLE.RLE(imgzigzag[1])
    # compressedImg_Cb, Tc_Cb = Huffman.Huffman(RLE_Cb)
    #
    # RLE_Cr, To_RLE_Cr = RLE.RLE(imgzigzag[2])
    # compressedImg_Cr, Tc_Cr = Huffman.Huffman(RLE_Cr)
    #
    # print("RLE_Y", RLE_Y)
    # print("RLE_Cb", RLE_Cb)
    # print("RLE_Cr", RLE_Cr)
    #
    # print(To_RLE_Y + To_RLE_Cb + To_RLE_Cr, Tc_Y + Tc_Cb + Tc_Cr)
    # Image.fromarray((imgzigzag)).show()

    # print("image originale :", imageJPG)
    # print("rgb2ycbcr", rgb2ycbcr(imageJPG))
    # print("ycbcr2rgb", ycbcr2rgb(rgb2ycbcr(imageJPG)))
    # splitIntoBlock16x16(imageJPG)
