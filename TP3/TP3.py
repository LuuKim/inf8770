import cv2
import math
import numpy as np
import matplotlib.pyplot as plt

"""
This function should process the video file.
Input: 
- file: the path to the video
Output:
- cut: vector of frame indices where cuts are detected
- grad: vector of tuples (start, end) of frame indices where gradations are detected
"""
# seuil pour 009
#CUT_SEUIL = 120000

#seuil pour 005

def process_video(file: str):
	cut=[]
	grad=[]
	histogrammeDifference = []
	listgrad = []
	listtemp = []
	cap = cv2.VideoCapture(file)
	index = 0
	while(True):
		ret, frame = cap.read()
		if ret:
			#TODO : frame processing algorithm
			actualpositionHistogramme = quantifyHistogramme(histogrammeCouleur(frame))
			if index >= 1:
				temp = int(differenceTrame(actualpositionHistogramme, lastpositionHistogramme))
				histogrammeDifference.append(temp)
				if temp > CUT_SEUIL:
					cut.append(index - 1)
				else:
					if temp > GRAD_SEUIL:
						listgrad.append(lastpositionHistogramme)
						if len(listgrad) == 1:
							indexGrad = index - 1
					else:
						size = len(listgrad)
						if size > 5:
							grad.append([indexGrad, index - 1 ])
							listgrad.clear()
			lastpositionHistogramme = actualpositionHistogramme
			index = index + 1
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		else:
			break
	cap.release()
	cv2.destroyAllWindows()
	plt.plot(histogrammeDifference, color='b')
	plt.xlim([0, len(histogrammeDifference)])
	plt.ylim(0, 800)
	#plt.show()
	return cut, grad


# if index >= 1:
# 	temp = int(differenceTrame(actualpositionHistogramme, lastpositionHistogramme))
# 	histogrammeDifference.append(temp)
# 	if temp > CUT_SEUIL and len(listgrad) < 1:
# 		cut.append(index - 1)
# 	if temp < CUT_SEUIL:
# 		if temp > GRAD_SEUIL:
# 			listgrad.append(lastpositionHistogramme)
# 			if len(listgrad) == 1:
# 				indexGrad = index
# 		if temp < GRAD_SEUIL:
# 			temp = differenceTrame(listgrad[0], listgrad[len(listgrad) - 1])
# 			if temp > TAUX:
# 				grad.append((indexGrad, index))
# 				listgrad.clear()
#
# 	lastpositionHistogramme = actualpositionHistogramme
# 	index = index + 1

"""
This function provides the ground truth for a given video file.
Input: 
- file: the path to the video
Output:
- cut: vector of frame indices where cuts happen
- grad: vector of tuples (start, end) of frame indices where gradations happen
"""
def read_groundtruth(file: str):
	cut=[]
	grad=[]
	f = open(file, "r")
	line = f.readline().split()
	while(line != []):
		if len(line)==1:
			cut.append(int(line[0]))
		elif len(line)==2:
			grad.append([int(line[0]), int(line[1])])
		line = f.readline().split()
	return cut, grad
#TODO : results analysis


# https://docs.opencv.org/3.4/d6/dc7/group__imgproc__hist.html
def histogrammeCouleur(capture):
	# Nb d'occurences de chaque couleur dans l'image -> 3 histogrammes : R, G, B.
	tempHistoR = cv2.calcHist([capture], [0], None, [8], [0, 256])
	tempHistoG = cv2.calcHist([capture], [1], None, [4], [0, 256])
	tempHistoB = cv2.calcHist([capture], [2], None, [4], [0, 256])

	#Concaténation des histogrammes
	histogrammeC = np.concatenate([tempHistoR, tempHistoG, tempHistoB], axis=None, out=None)
	# plt.plot(tempHistoR, color='r')
	# plt.plot(tempHistoG, color='g')
	# plt.plot(tempHistoB, color='b')
	# plt.plot(histogrammeC, color='black')
	# plt.xlim([0, 800])
	#plt.show()
	return histogrammeC

def differenceTrame(hcouleur1, hcouleur2):
	#difference entre histogramme de la trame courante et la trame précédente
	differenceT = np.square(hcouleur1 - hcouleur2).sum()
	differenceT = math.sqrt(differenceT)
	# comparaison de 2 histogrammes avec la distance euclidienne
	differenceHisto = cv2.compareHist(np.array(hcouleur1), np.array(hcouleur2), cv2.HISTCMP_CHISQR)

	return differenceHisto

def quantifyHistogramme(histogramme):
	return histogramme


def compareValue(cut , gt_cut):
	same_values = []
	count = 0
	for i in cut:
		for j in gt_cut:
			if i == j:
				count = count+1
				same_values.append(i)
	diff = len(gt_cut) - count
	return count, same_values

SEUIL_TEST  = [100, 200, 300, 400, 500, 750, 1000, 2000, 3000, 4000, 5000, 5500]
for i in SEUIL_TEST:
	print(i)
	CUT_SEUIL = i
	GRAD_SEUIL = 10000
	cut, grad = process_video('NAD57.mpg')
	gt_cut, gt_grad = read_groundtruth('nad57.txt')
	# print("taille  trouvee pour cut:", len(cut))
	compteur, valeur = compareValue(cut, gt_cut)
	# print("cut: ",valeur)
	# print ("valeur en trop", (len(cut) - len(gt_cut)))
	print("pourcentage de similitude: " , (compteur/len(cut)) * 100 )
	print("taux: ", (compteur/len(gt_cut)) * 100)
# print("taille a trouver pour grad:", len(gt_grad))
# compteur, valeur = compareValue(grad, gt_grad)
# print("cut: ",valeur)
# print("valeur communes trouvées : ", compteur)
# print ("valeur en trop", (len(grad) - len(gt_grad)))
# print("FP : ", len(grad) - compteur)
	print("valeur communes trouvées : ", compteur)
	print("cut", len(cut))
	print("FP : ", len(cut) - compteur)
	print("gtcut" , len(gt_cut))
