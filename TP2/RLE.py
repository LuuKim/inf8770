# https://github.com/gabilodeau/INF8770/blob/master/Codage%20par%20plage.ipynb

import numpy as np

from Utils import countBits, most_frequent, arrayOfIntToString, arrayOfStringToInt, findElement, arrayOfStringToFloat


#  * Codage par plage
#  https://github.com/gabilodeau/INF8770/blob/master/Codage%20par%20plage.ipynb
#  * @param length (longueur de chaîne désirée)
def RLE(Message):
    toDecode = []
    RLECompressed = []
    Originales = []
    Message = Message.tolist()
    max = Message.count(most_frequent(Message))
    compteur = countBits(max)
    Message = arrayOfIntToString(Message)
    dictsymb = [Message[0]]
    dictbin = ["{:b}".format(0)]
    nbsymboles = 1
    for i in range(1, len(Message)):
        if Message[i] not in dictsymb:
            dictsymb += [Message[i]]
            dictbin += ["{:b}".format(nbsymboles)]
            nbsymboles += 1

    longueurOriginale = np.ceil(np.log2(nbsymboles)) * len(Message)  # Longueur du message avec codage binaire

    for i in range(nbsymboles):
        dictbin[i] = "{:b}".format(i).zfill(int(np.ceil(np.log2(nbsymboles))))

    dictsymb.sort()
    dictionnaire = np.transpose([dictsymb, dictbin])
    #print(dictionnaire)

    i = 0
    MessageCode = []
    longueur = 0
    while i < len(Message):
        carac = Message[i]  # caractere qui sera codé
        repetition = 1
        # Calcul le nombre de répétitions.
        i += 1
        # tient compte de la limite du compteur
        while i < len(Message) and repetition < 2 ** compteur and Message[i] == carac:
            i += 1
            repetition += 1
        # Codage à l'aide du dictionnaire
        coderepetition = "{:b}".format(repetition - 1).zfill(compteur)
        codebinaire = dictbin[dictsymb.index(carac)]
        MessageCode += [coderepetition, codebinaire]
        longueur += len(codebinaire) + len(coderepetition)

    # print("Longueur = {0}".format(longueur))
    # print("Longueur originale = {0}".format(longueurOriginale))

    nvMsg = []
    i = 0
    while i < (len(MessageCode)-1):
        nvMsg.append(MessageCode[i] + MessageCode[i+1])
        i = i + 2

    MessageCode = nvMsg

    return MessageCode, longueurOriginale, compteur, dictionnaire

def undoRLE(msg, compteur, dic):
    msgDecoded = []
    dicCpt = []
    for i in range(compteur+1):
        dicCpt.append([i+1, "{:b}".format(i).zfill(compteur)])


    for i in range(len(msg)):
        sizeSymbol = len(dic[0][1])
        repetition = ""
        symbol = ""
        for j in range(compteur):
            repetition += msg[i][j]

        #print("repetition", repetition)
        repetition = findElement(dicCpt, repetition)

        for j in range(sizeSymbol):
            symbol += msg[i][j+compteur]

        #print("symbol", symbol)
        symbol = findElement(dic, symbol)

        for j in range(repetition):
            msgDecoded.append(symbol)

    msgDecoded = np.array(msgDecoded)
    msgDecoded = arrayOfStringToFloat(msgDecoded)
    return msgDecoded