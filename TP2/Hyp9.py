# coding: latin-1
########################## IMPORTS #########################
import random
import numpy as np
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np

import scipy.fftpack as dctpack
from PIL import Image


####################### CONSTANTES ###############################

ADJUST_COLOR = 128
MAX_COLOR = 255
AMOUNT_Y = AMOUNT_CB = AMOUNT_CR = 0
BLOC_SIZE = 8
SIZE = 8
BLOC_Y = BLOC_CB = BLOC_CR = []
Blocs = []

#https://github.com/gabilodeau/INF8770/blob/master/Transformee%20DCT%20et%20quantification.ipynb
QUANTIFICATION_MATRIX = np.array([[16, 11, 10, 16, 24, 40, 51, 61],
                                 [12, 12, 14, 19, 26, 58, 60, 55],
                                 [14, 13, 16, 24, 40, 57, 69, 56],
                                 [14, 17, 22, 29, 51, 87, 80, 62],
                                 [18, 22, 37, 56, 68, 109, 103, 77],
                                 [24, 35, 55, 64, 81, 104, 103, 92],
                                 [49, 64, 78, 77, 103, 121, 120, 101],
                                 [72, 92, 95, 98, 112, 100, 103, 99]])
arrayRgb2YCbCr = [[.299, .587, .114], [-.1687, -.3313, .4997], [.4997, -.4187, -.0813]]
arrayYCbCr2Rgb = [[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]]
nameImageOriginal = "img.jpg"


############################################################

# https://stackoverflow.com/questions/34913005/color-space-mapping-ycbcr-to-rgb
def rgb2ycbcr(image):
    imgArray = np.array(image)
    xform = np.array(arrayRgb2YCbCr)
    ycbcr = imgArray.dot(xform.T)
    ycbcr[:,:,[1,2]] += ADJUST_COLOR
    return np.uint8(ycbcr)

# https://stackoverflow.com/questions/34913005/color-space-mapping-ycbcr-to-rgb
def ycbcr2rgb(im):
    xform = np.array(arrayYCbCr2Rgb)
    rgb = im.astype(np.float)
    rgb[:,:,[1,2]] -= ADJUST_COLOR
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb > MAX_COLOR, MAX_COLOR)
    np.putmask(rgb, rgb < 0, 0)
    return np.uint8(rgb)

# https://medium.com/@sddkal/chroma-subsampling-in-numpy-47bf2bb5af83
def chroma(arrayYCbCr):
    original = arrayYCbCr.copy()
    arrayYCbCr[1::2, :] = arrayYCbCr[::2, :]
    # Vertically, every second element equals to element above itself.
    arrayYCbCr[:, 1::2] = arrayYCbCr[:, ::2]
    # Horizontally, every second element equals to the element on its left side.
    print(arrayYCbCr[0][0])
    return arrayYCbCr


def splitIntoBlock16x16(img):
        w_size = img.shape[0]
        h_size = img.shape[1]
        for w in range(0, w_size - SIZE + 1, SIZE):
            for h in range(0, h_size - SIZE + 1, SIZE):
                Blocs.append(img[w:w+SIZE, h:h+SIZE])
        for b in Blocs:
            BLOC_Y = b[:, :, 0]
            BLOC_CB = b[:, :, 1]
            BLOC_CR = b[:, :, 2]
            #print(BLOC_Y[0])
        print(BLOC_Y[0][0])
        print(BLOC_CB[0][0])
        print(BLOC_CR[0][0])
        return img

def applyDCT(img):
    for b in Blocs:
        BLOC_Y = b[:, :, 0]
        BLOC_CB = b[:, :, 1]
        BLOC_CR = b[:, :, 2]
        DCT(BLOC_Y)
        DCT(BLOC_CB)
        DCT(BLOC_CR)
    print(BLOC_Y[0][0])
    print(BLOC_CB[0][0])
    print(BLOC_CR[0][0])
    return img

def applyIDCT(img):
    for b in Blocs:
        BLOC_Y = b[:, :, 0]
        BLOC_CB = b[:, :, 1]
        BLOC_CR = b[:, :, 2]
        IDCT(BLOC_Y)
        IDCT(BLOC_CB)
        IDCT(BLOC_CR)
    print(BLOC_Y[0][0])
    print(BLOC_CB[0][0])
    print(BLOC_CR[0][0])
    return img

def applyQuantification(img):
    for b in Blocs:
        BLOC_Y = b[:, :, 0]
        BLOC_CB = b[:, :, 1]
        BLOC_CR = b[:, :, 2]
        BLOC_Y = quantification(BLOC_Y)
        BLOC_CB = quantification(BLOC_CB)
        BLOC_CR = quantification(BLOC_CR)
    print(BLOC_Y[0][0])
    print(BLOC_CB[0][0])
    print(BLOC_CR[0][0])
    return img

def applyInvQuantification(img):
    for b in Blocs:
        BLOC_Y = b[:, :, 0]
        BLOC_CB = b[:, :, 1]
        BLOC_CR = b[:, :, 2]
        BLOC_Y = invQuantification(BLOC_Y)
        BLOC_CB = invQuantification(BLOC_CB)
        BLOC_CR = invQuantification(BLOC_CR)
    print(BLOC_Y[0][0])
    print(BLOC_CB[0][0])
    print(BLOC_CR[0][0])
    return img

def applyZigZag(img):
    for b in Blocs:
        BLOC_Y = b[:, :, 0]
        BLOC_CB = b[:, :, 1]
        BLOC_CR = b[:, :, 2]
        BLOC_Y = block_to_zigzag(BLOC_Y)
        BLOC_CB = block_to_zigzag(BLOC_CB)
        BLOC_CR = block_to_zigzag(BLOC_CR)
    print(BLOC_Y)
    return img


def quantification(imageBlocDCT):
    return np.round(np.divide(imageBlocDCT, QUANTIFICATION_MATRIX))

def invQuantification(imageBlocDCT):
    return imageBlocDCT * QUANTIFICATION_MATRIX

def DCT(bloc):
    # On soustrait 128 pour avoir en signal oscillant autour de z�ro
    bloc[:] = bloc[:] - 128
    DCT = dctpack.dct(dctpack.dct(bloc[:], axis=0, norm='ortho'), axis=1, norm='ortho')
    np.set_printoptions(precision=2)
    np.set_printoptions(suppress=True)
    return DCT

def IDCT(img):
    # On soustrait 128 pour avoir en signal oscillant autour de z�ro
    IdCT = dctpack.idct(dctpack.idct(img[:], axis=0, norm='ortho'), axis=1, norm='ortho')
    np.set_printoptions(precision=2)
    np.set_printoptions(suppress=True)
    img[:] = img[:] + 128
    return IdCT


# https://github.com/ghallak/jpeg-python/blob/2fe1bd2244c3090543695b106866dfa0a3b48f6c/encoder.py#L16
def block_to_zigzag(block):
    return np.array([block[point] for point in zigzag_points(*block.shape)])

# https://github.com/ghallak/jpeg-python/blob/2fe1bd2244c3090543695b106866dfa0a3b48f6c/encoder.py#L16
def zigzag_points(rows, cols):
    # constants for directions
    UP, DOWN, RIGHT, LEFT, UP_RIGHT, DOWN_LEFT = range(6)

    # move the point in different directions
    def move(direction, point):
        return {
            UP: lambda point: (point[0] - 1, point[1]),
            DOWN: lambda point: (point[0] + 1, point[1]),
            LEFT: lambda point: (point[0], point[1] - 1),
            RIGHT: lambda point: (point[0], point[1] + 1),
            UP_RIGHT: lambda point: move(UP, move(RIGHT, point)),
            DOWN_LEFT: lambda point: move(DOWN, move(LEFT, point))
        }[direction](point)

    # return true if point is inside the block bounds
    def inbounds(point):
        return 0 <= point[0] < rows and 0 <= point[1] < cols

    # start in the top-left cell
    point = (0, 0)

    # True when moving up-right, False when moving down-left
    move_up = True

    for i in range(rows * cols):
        yield point
        if move_up:
            if inbounds(move(UP_RIGHT, point)):
                point = move(UP_RIGHT, point)
            else:
                move_up = False
                if inbounds(move(RIGHT, point)):
                    point = move(RIGHT, point)
                else:
                    point = move(DOWN, point)
        else:
            if inbounds(move(DOWN_LEFT, point)):
                point = move(DOWN_LEFT, point)
            else:
                move_up = True
                if inbounds(move(DOWN, point)):
                    point = move(DOWN, point)
                else:
                    point = move(RIGHT, point)



if __name__ == "__main__":

    imageJPG = (Image.open(nameImageOriginal))

    imageBis = rgb2ycbcr(imageJPG)

    #Image.fromarray(imageBis).show()

    imgSplit = splitIntoBlock16x16(chroma(imageBis))

    #Image.fromarray(imgSplit).show()

    imgdct = applyDCT(imgSplit)

    #Image.fromarray(imgSplit).show()

    imgquant = applyQuantification(imgdct)

    imgzigzag = applyZigZag(imgquant)

    Image.fromarray(imgzigzag).show()



    #print("image originale :", imageJPG)
    #print("rgb2ycbcr", rgb2ycbcr(imageJPG))
    #print("ycbcr2rgb", ycbcr2rgb(rgb2ycbcr(imageJPG)))
    #splitIntoBlock16x16(imageJPG)